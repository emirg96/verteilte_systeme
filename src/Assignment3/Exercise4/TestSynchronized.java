package Assignment3.Exercise4;

import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class TestSynchronized {

    public static void main(String[] args) throws InterruptedException {
        new Thread(() -> {
            try {
                StringOpsServer.main(new String[0]);
            } catch (RemoteException e) {
                e.printStackTrace();
            } catch (AlreadyBoundException e) {
                e.printStackTrace();
            }
        }).start();

        Thread.sleep(2000);

        for (int i = 0; i < 3; i++) {
            new Thread(() -> {
                try {
                    StringOpsClient.main(new String[0]);
                } catch (RemoteException e) {
                    e.printStackTrace();
                } catch (NotBoundException e) {
                    e.printStackTrace();
                }
            }).start();
        }

        // Last 2 console output lines:
        // 15000:us. for file this prepared have should professor the think I
        // It took 3212814915 ns to process the file!
    }

}
