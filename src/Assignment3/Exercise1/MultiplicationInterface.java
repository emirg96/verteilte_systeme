package Assignment3.Exercise1;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface MultiplicationInterface extends Remote {

    int REMOTE_PORT = 55555;
    String REMOTE_HOST = "localhost";
    String REMOTE_ID = "MultiplicationServer";

    int mult(int a, int b) throws RemoteException;

}
