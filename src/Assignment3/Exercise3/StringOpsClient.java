package Assignment3.Exercise3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class StringOpsClient {

    public static void main(String[] args) throws RemoteException, NotBoundException {
        Registry registry = LocateRegistry.getRegistry(StringOpsInterface.REMOTE_HOST, StringOpsInterface.REMOTE_PORT);
        StringOpsInterface stringOpsObject = (StringOpsInterface) registry.lookup(StringOpsInterface.REMOTE_ID);

        long start = System.nanoTime();
        try {
            File file = new File("src/Assignment3/Exercise3/text.txt");
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            StringBuffer stringBuffer = new StringBuffer();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                System.out.println(stringOpsObject.uniqueReverse(line));
            }
            fileReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("It took " + (System.nanoTime() - start) + " ns to process the file!");
        // It took 1396226812 ns to process the file!
    }
}
