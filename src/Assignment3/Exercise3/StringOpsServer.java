package Assignment3.Exercise3;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;

public class StringOpsServer extends UnicastRemoteObject implements StringOpsInterface {

    private int identifier = 1;

    public StringOpsServer(int port) throws RemoteException {
        super(port);
    }

    @Override
    public String uniqueReverse(String input) throws RemoteException {
        List<String> wordsList = Arrays.asList(input.split(" "));
        Collections.reverse(wordsList);
        return identifier++ + ":" + String.join(" ", wordsList.toArray(new String[0]));
    }

    public static void main(String[] args) throws RemoteException, AlreadyBoundException {
        StringOpsServer stringOpsServer = new StringOpsServer(REMOTE_PORT);
        Registry registry = LocateRegistry.createRegistry(REMOTE_PORT);
        registry.bind(REMOTE_ID, stringOpsServer);
    }
}
