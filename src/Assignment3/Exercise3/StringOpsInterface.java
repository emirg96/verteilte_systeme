package Assignment3.Exercise3;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface StringOpsInterface extends Remote {

    int REMOTE_PORT = 55557;
    String REMOTE_HOST = "localhost";
    String REMOTE_ID = "StringOpsServer";

    String uniqueReverse(String input) throws RemoteException;
}
