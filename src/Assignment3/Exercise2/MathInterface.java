package Assignment3.Exercise2;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.security.InvalidParameterException;

public interface MathInterface extends Remote {

    int REMOTE_PORT = 55556;
    String REMOTE_HOST = "localhost";
    String REMOTE_ID = "MathServer";

    String supportedOperations() throws RemoteException;

    int mult(int... factors) throws RemoteException;

    int sub(int... summands) throws RemoteException;

    int add(int... addends) throws RemoteException, InvalidParameterException;

}
