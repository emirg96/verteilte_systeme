package Assignment3.Exercise2;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.security.InvalidParameterException;

public class MathServer extends UnicastRemoteObject implements MathInterface {

    protected MathServer() throws RemoteException {
    }

    protected MathServer(int port) throws RemoteException {
        super(port);
    }

    /**
     * Returns a list of available operations. Most probably misunderstood the task:
     * "When the client is connected to the server, the server should provide list of all available operations."
     * That doesn't make any sense to me because client has the interface, too. Otherwise he wouldn't be able to communicate with the server.
     * In my opinion this function should be left out.
     *
     * @return
     */
    @Override
    public String supportedOperations() {
        String supportedOperations = "";
        supportedOperations += "Multiplication: mult(int... factors) \n";
        supportedOperations += "Subtraction: sub(int... subtrahends) \n";
        supportedOperations += "Addition: add(int... addends)";
        return supportedOperations;
    }

    /**
     * Returns a product of all factors. If no parameters given throws an InvalidParameterException.
     *
     * @param factors
     * @return
     * @throws RemoteException
     * @throws InvalidParameterException
     */
    @Override
    public int mult(int... factors) throws RemoteException, InvalidParameterException {
        if (factors.length == 0)
            throw new InvalidParameterException();

        int product = 1;
        for (int factor : factors) {
            product *= factor;
        }
        return product;
    }

    /**
     * Returns a difference of all subtrahends except the first one because it is considered as minuend.
     *
     * @param subtrahends
     * @return
     * @throws RemoteException
     */
    @Override
    public int sub(int... subtrahends) throws RemoteException {
        if (subtrahends.length == 0)
            return 0;

        int difference = 2 * subtrahends[0]; // The first operand for subtraction is not a subtrahend, it is a minuend.
        for (int subtrahend : subtrahends) {
            difference -= subtrahend;
        }
        return difference;
    }

    /**
     * Returns a sum of all addends.
     *
     * @param addends
     * @return
     * @throws RemoteException
     */
    @Override
    public int add(int... addends) throws RemoteException {
        int sum = 0;
        for (int addend : addends) {
            sum += addend;
        }
        return sum;
    }

    public static void main(String[] args) throws RemoteException, AlreadyBoundException {
        MathServer mathServer = new MathServer();
        Registry registry = LocateRegistry.createRegistry(REMOTE_PORT);
        registry.bind(REMOTE_ID, mathServer);
    }
}
