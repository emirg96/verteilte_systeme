package Assignment3.Exercise2;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class MathClient {

    public static void main(String[] args) throws RemoteException, NotBoundException {
        Registry registry = LocateRegistry.getRegistry(MathInterface.REMOTE_HOST, MathInterface.REMOTE_PORT);
        MathInterface multiplicationObject = (MathInterface) registry.lookup(MathInterface.REMOTE_ID);

        System.out.println("Supported operations:\n" + multiplicationObject.supportedOperations() + "\n");

        System.out.println("multiplicationObject.mult(5, 25, 10) = " + multiplicationObject.mult(5, 25, 10));

        System.out.println("multiplicationObject.add(4, 5, 6, 7, 10) = " + multiplicationObject.add(4, 5, 6, 7, 10));

        System.out.println("multiplicationObject.sub(100, 50, 20, 15, 10) = " + multiplicationObject.sub(100, 50, 20, 15, 10));
    }
}
