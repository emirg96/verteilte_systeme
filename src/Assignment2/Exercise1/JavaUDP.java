package Assignment2.Exercise1;

import java.io.IOException;
import java.net.SocketException;
import java.net.UnknownHostException;

public class JavaUDP {

    public static void main(String[] args) throws IOException, InterruptedException {
        QuotationServer quotationServer = new QuotationServer(5555);
        Thread quotationThread = new Thread(quotationServer);
        quotationThread.start();

        QuotationClient quotationClient = new QuotationClient("localhost", 5555);
        String answer = quotationClient.invoke();
        System.out.println("[CLIENT] Server answers: " + answer);
    }

}
