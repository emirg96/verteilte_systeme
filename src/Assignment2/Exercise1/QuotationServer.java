package Assignment2.Exercise1;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Random;

/**
 * @author Emir Goran
 */
public class QuotationServer implements Runnable {
    private static final int BUFFER_SIZE = 256;
    private static final String[] QUOTES = new String[]
            {
                    "So what if I don’t know what “Armageddon” means? It’s not the end of the world.",
                    "Don’t you hate it when someone answers their own questions? I do.",
                    "Do I lose when the police officer says papers and I say scissors?",
                    "A lot of people cry when they cut onions. The trick is not to form an emotional bond.",
                    "Life is like a box of chocolates. It doesn’t last long if you’re fat.",
                    "Do not go to the bathroom in a dream. It’s a trap!",
            };

    private DatagramSocket socket;
    private byte[] buf = new byte[BUFFER_SIZE];

    private QuotationServer() {
    }

    /**
     * Create new DatagramSocket on a given port
     *
     * @param port
     * @throws SocketException
     */
    public QuotationServer(int port) throws SocketException {
        socket = new DatagramSocket(port);
    }

    public void run() {
        DatagramPacket packet = new DatagramPacket(buf, buf.length); // Create new DatagramPacket for receiving/sending packets over DatagramSocket

        // Waiting for a client to invoke me (server)
        try {
            socket.receive(packet);
        } catch (IOException e) {
            System.err.println("Couldn't receive the packet!");
        }

        InetAddress address = packet.getAddress();
        int port = packet.getPort();

        String messageToSend = QUOTES[new Random().nextInt(QUOTES.length)];
        packet = new DatagramPacket(messageToSend.getBytes(), messageToSend.getBytes().length, address, port);
        System.out.println("[SERVER] Received request from: " + address.toString() + ":" + port + ". Sending him a random quote...");

        // Try to give an answer to the client
        try {
            socket.send(packet);
        } catch (IOException e) {
            System.err.println("Couldn't send the packet!");

        }
        socket.close();
    }

}
