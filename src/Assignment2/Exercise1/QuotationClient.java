package Assignment2.Exercise1;

import java.io.IOException;
import java.net.*;

public class QuotationClient {
    private static final int BUFFER_SIZE = 256;

    private DatagramSocket socket;
    private InetAddress address;
    private int port;

    private QuotationClient() {
    }

    public QuotationClient(String host, int port) throws SocketException, UnknownHostException {
        this.port = port;
        socket = new DatagramSocket();
        address = InetAddress.getByName(host);
    }

    public String invoke() throws IOException {
        // Invoke a server by sending an empty string to it
        byte[] sendBuffer = "".getBytes();
        DatagramPacket packet = new DatagramPacket(sendBuffer, sendBuffer.length, address, port);
        socket.send(packet);

        // Receive an answer from the server (after invoking the server we are expecting an answer)
        byte[] receiveBuffer = new byte[BUFFER_SIZE];
        packet = new DatagramPacket(receiveBuffer, BUFFER_SIZE);
        socket.receive(packet);
        String received = new String(packet.getData(), 0, packet.getLength());
        return received;
    }

    public void close() {
        socket.close();
    }
}