package Assignment2.Exercise3;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MulticastDateServer extends Thread {
    protected MulticastSocket socket = null;
    protected byte[] buffer = new byte[256];
    protected InetAddress groupAddress = null;

    private static int serverID = 0;

    private MulticastDateServer() {
    }

    /**
     * Join multicast group with given port and address.
     *
     * @param port
     * @param address
     * @throws IOException
     */
    public MulticastDateServer(int port, String address) throws IOException {
        socket = new MulticastSocket(port);
        socket.setReuseAddress(true);
        groupAddress = InetAddress.getByName(address);
        socket.joinGroup(groupAddress);
        serverID++;
    }

    /**
     * Server thread.
     */
    public void run() {
        try {

            while (true) {
                // Receive (expect) a packet using a predefined buffer.
                DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
                socket.receive(packet);

                // Get address and port of an invoker.
                InetAddress address = packet.getAddress();
                int port = packet.getPort();

                // Read the message from the invoker.
                String receivedMessage = new String(packet.getData(), 0, packet.getLength());
                System.out.println("receivedMessage = " + receivedMessage);

                // Process the invoker's message and if valid give an answer.
                // The message should have following format: REQ#PORT# where port is invoker's port number.
                String pattern = "(REQ#)(\\d+)(#)";
                if (receivedMessage.matches(pattern)) {
                    Pattern regexPattern = Pattern.compile(pattern);
                    Matcher patternMatcher = regexPattern.matcher(receivedMessage);
                    System.out.println("clientPort = " + patternMatcher.group(1));
                    int clientPort = Integer.parseInt(patternMatcher.group(1));

                    String datetimeResponse = "TS-" + serverID + "#" + new Date().getTime() + "#";
                    packet = new DatagramPacket(datetimeResponse.getBytes(), datetimeResponse.length(), address, port);
                    socket.send(packet);
                }
            }

            // socket.leaveGroup(groupAddress);
            // socket.close();

        } catch (Exception e) {
        }

    }

}
