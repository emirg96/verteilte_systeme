package Assignment2.Exercise3;

import java.io.IOException;
import java.net.*;

public class MulticastDateClient {

    private DatagramSocket socket;
    private InetAddress groupAddress;
    private static long timeout = 1000; // 1000 ms timeout
    private static final int BUFFER_SIZE = 256;

    private MulticastDateClient() {
    }

    public MulticastDateClient(int groupPort, String groupAddress) throws IOException {
        this.socket = new DatagramSocket();
        this.groupAddress = InetAddress.getByName(groupAddress);

        // Invoke a server by sending an empty string to it
        byte[] sendBuffer = ("REQ#" + 7777 + "#").getBytes();
        DatagramPacket sendPacket = new DatagramPacket(sendBuffer, sendBuffer.length, this.groupAddress, groupPort);
        socket.send(sendPacket);

        long startTime = System.currentTimeMillis();

        while (System.currentTimeMillis() < startTime + timeout) {
            byte[] receiveBuffer = new byte[BUFFER_SIZE];
            DatagramPacket packet = new DatagramPacket(receiveBuffer, BUFFER_SIZE);
            socket.receive(packet);
            String received = new String(packet.getData(), 0, packet.getLength());

            System.out.println(received);
        }
    }

    public static void main(String[] args) throws IOException {
        MulticastDateClient multicastDateClient = new MulticastDateClient(6666, "230.0.0.0");
    }
}
