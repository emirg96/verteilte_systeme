package Assignment2.Exercise2;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * NOT FINISHED / NOT WORKING!
 */
public class FileServer implements Runnable {

    private final static File PUBLIC_DIRECTORY = new File("Server Directory");

    private int port;
    private ServerSocket serverSocket;
    private Socket client;

    private FileServer() {
    }

    private FileServer(ServerSocket serverSocket) {
        this.serverSocket = serverSocket;

        Thread thread = new Thread(this);
        thread.start();
    }

    public FileServer(int port) {
        this.port = port;

        try {
            this.serverSocket = new ServerSocket(this.port);

            Thread thread = new Thread(this);
            thread.start();

        } catch (Exception e) {
            System.out.println("FileServer: Couldn't start the server at this port: " + this.port);
        }
    }

    @Override
    public void run() {
        try {
            this.client = serverSocket.accept();

            // Listen to a new client after accepting the current one
            FileServer fileServer = new FileServer(serverSocket);

            BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
            PrintWriter out = new PrintWriter(client.getOutputStream(), true);

            String inputLine;

            if ((inputLine = in.readLine()) != null) {
                out.println(FileProtocol.processInput(inputLine, PUBLIC_DIRECTORY));
            }

            this.client.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
