package Assignment2.Exercise2;

import java.io.*;
import java.net.Socket;

/**
 * NOT FINISHED / NOT WORKING!
 */
public class FileClient implements Runnable {

    private final static File CLIENT_DIRECTORY = new File("Client Directory");

    private String host;
    private int port;
    private String command;

    private FileClient() {
    }

    public FileClient(String host, int port, String command) {
        this.host = host;
        this.port = port;
        this.command = command;

        Thread thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        try {
            Socket socket = new Socket(this.host, this.port);

            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

            out.println(this.command);

            String inputLine;
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            if ((inputLine = in.readLine()) != null) {
                System.out.println("FileClient: Answer from server on query '" + this.command + "': " + inputLine);
            }

            socket.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
