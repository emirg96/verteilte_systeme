package Assignment2.Exercise2;

import java.io.File;
import java.util.ArrayList;

/**
 * NOT FINISHED / NOT WORKING!
 */
public class FileProtocol {

    public static String processInput(String input, File directory) {
        String command;
        String parameter;
        try {
            command = input.split(" ")[0];
            parameter = input.split(" ")[1];
        } catch (Exception e) {
            command = input;
            parameter = "";
        }

        switch (command) {
            case "list":
                return getFilesNames(getFilesOnly(directory));
            case "get": {
                if (fileExists(parameter, directory)) {
                    File file = getFile(parameter, directory);
                    return "Name: " + file.getName() + " Size: " + file.length() + " Bytes";
                } else return "File with given name doesn't exist!";
            }
            default:
                return "Unknown command: " + input;
        }
    }

    public static boolean createDirectoryIfNotExist(File file) {
        if (!file.exists()) {
            return file.mkdir();
        }
        return true;
    }

    public static File[] getFilesOnly(File directory) {
        File[] listOfFiles = directory.listFiles();

        ArrayList<File> filesOnlyList = new ArrayList<>();

        for (File file : listOfFiles)
            if (file.isFile())
                filesOnlyList.add(file);

        File[] filesOnly = new File[filesOnlyList.size()];

        int i = 0;
        for (File file : filesOnlyList)
            filesOnly[i++] = file;

        return filesOnly;
    }

    public static String getFilesNames(File[] files) {
        String filesList = "";

        for (File file : files) {
            filesList += file.getName() + "     ";
        }

        return filesList;
    }

    public static boolean fileExists(String fileName, File inDirectory) {
        File[] files = getFilesOnly(inDirectory);
        boolean exists = false;

        for (File file : files) {
            if (fileName.equals(file.getName()))
                exists = true;
        }

        return exists;
    }

    public static File getFile(String fileName, File inDirectory) {
        File[] files = getFilesOnly(inDirectory);
        File returnFile = new File(fileName);

        for (File file : files) {
            if (fileName.equals(file.getName()))
                returnFile = file;
        }

        return returnFile;
    }
}
