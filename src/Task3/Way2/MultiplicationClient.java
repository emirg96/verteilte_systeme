package Task3.Way2;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class MultiplicationClient {

    public static void main(String[] args) throws RemoteException, NotBoundException {
        Registry registry = LocateRegistry.getRegistry(MultiplicationInterface.REMOTE_HOST, MultiplicationInterface.REMOTE_PORT);
        MultiplicationInterface multiplicationObject = (MultiplicationInterface) registry.lookup(MultiplicationInterface.REMOTE_ID);

        System.out.println("multiplicationObject.mult(5, 25) = " + multiplicationObject.mult(5, 25));
    }
}
