package Task3.Way2;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class MultiplicationServer extends UnicastRemoteObject implements MultiplicationInterface {

    protected MultiplicationServer() throws RemoteException {
    }

    protected MultiplicationServer(int port) throws RemoteException {
        super(port);
    }

    @Override
    public int mult(int a, int b) throws RemoteException {
        return a * b;
    }

    public static void main(String[] args) throws RemoteException, AlreadyBoundException {
        MultiplicationServer multiplicationServer = new MultiplicationServer();
        Registry registry = LocateRegistry.createRegistry(REMOTE_PORT);
        registry.bind(REMOTE_ID, multiplicationServer);
    }
}
