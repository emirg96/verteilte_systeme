package Task3.Way1;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface MultiplicationInterface extends Remote {

    public static final int REMOTE_PORT = 55555;
    public static final String REMOTE_HOST = "localhost";
    public static final String REMOTE_ID = "MultiplicationServer";

    public int mult(int a, int b) throws RemoteException;

}
