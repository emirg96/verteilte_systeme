package Task3.Way1;

import java.net.MalformedURLException;;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class MultiplicationServer extends UnicastRemoteObject implements MultiplicationInterface {

    protected MultiplicationServer() throws RemoteException {
    }

    protected MultiplicationServer(int port) throws RemoteException {
        super(port);
    }

    @Override
    public int mult(int a, int b) throws RemoteException {
        return a * b;
    }

    public static void main(String[] args) throws RemoteException, MalformedURLException {
        MultiplicationServer multiplicationServer = new MultiplicationServer();
        Naming.rebind("rmi://localhost/MultiplicationServer", multiplicationServer);
    }
}
