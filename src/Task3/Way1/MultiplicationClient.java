package Task3.Way1;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class MultiplicationClient {

    public static void main(String[] args) throws RemoteException, NotBoundException, MalformedURLException {
        MultiplicationInterface multiplicationObject = (MultiplicationInterface) Naming.lookup(MultiplicationInterface.REMOTE_ID);

        System.out.println("multiplicationObject.mult(5, 25) = " + multiplicationObject.mult(5, 25));
    }
}
