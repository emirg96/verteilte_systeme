package Task1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Task4_Server {

    public static void main(String[] args) {
        int port = 5555;

        try {
            ServerSocket serverSocket = new ServerSocket(port);
            Socket client = serverSocket.accept();

            System.out.println("client = " + client);

            PrintWriter out = new PrintWriter(client.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));

            String inputLine;

            if ((inputLine = in.readLine()) != null) {
                System.out.println("Message from client: " + inputLine);
            }

            System.out.println("Read all from client! Sending him confirmation message...");

            out.println("Hi Client, I've received your message!");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
