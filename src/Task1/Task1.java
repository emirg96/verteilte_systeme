package Task1;

public class Task1 implements Runnable {

    private static int i = 20;

    public static void main(String[] args) {

        System.out.println("Starting program...");

        Task1 task1 = new Task1();
        Thread t = new Thread(task1);
        t.start();

    }

    @Override
    public void run() {

        if (i-- > 0) {
            Task1 task1 = new Task1();
            Thread t = new Thread(task1);
            t.start();
            try {
                t.join();
                System.out.println("Hello World! This is thread #" + Thread.currentThread().getId());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
