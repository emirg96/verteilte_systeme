package Task1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Task4_Client {

    public static void main(String[] args) {
        String host = "localhost";
        int port = 5555;

        try {
            Socket socket = new Socket(host, port);

            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            out.println("Hello Server!");

            String inputLine;

            if ((inputLine = in.readLine()) != null) {
                System.out.println("Message from server: " + inputLine);
            }

            socket.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
