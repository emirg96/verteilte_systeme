package Task1;

public class Task2 implements Runnable {

    private static int i = 0;

    @Override
    public void run() {
        System.out.println("Hello World! This is thread #" + (i + 1));
        if (++i < 20) {
            Task2 task2 = new Task2();
            Thread thread = new Thread(task2);
            thread.start();
        }
    }

    public static void main(String[] args) {
        Task2 task2 = new Task2();
        Thread thread = new Thread(task2);
        thread.start();
    }
}
