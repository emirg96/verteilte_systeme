package Assignment5.Exercise2;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public class MathWSClient {
    private static MathInterfaceWS math;

    /**
     * Connection part - establish connection to server and set instance of MathInterfaceWS math accordingly
     */
    static {
        try {
            URL url = new URL("http://localhost:56789/ws/mathwsserver?wsdl");
            QName qName = new QName("http://Exercise2.Assignment5/", "MathWSServerService");
            Service service = Service.create(url, qName);
            math = service.getPort(MathInterfaceWS.class);
        } catch (Exception e) {
            System.err.println("Invalid URL or Server not available!");
        }
    }

    /**
     * Implementation of the function which calculates surface area of a cylinder
     */
    public static double calculateSurfaceAreaOfCylinder(double radius, double height) {
        double part1 = math.multiply(math.multiply(math.multiply(2, radius), radius), Math.PI);
        double part2 = math.multiply(math.multiply(math.multiply(2, radius), radius), height);
        return math.add(part1, part2);
    }

    /**
     * Main/Test function
     */
    public static void main(String[] args) throws MalformedURLException {
        double surfaceArea = calculateSurfaceAreaOfCylinder(5.21, 12.123);
        System.out.println("surfaceArea = " + surfaceArea);
    }
}
