package Assignment5.Exercise2;

import javax.jws.WebService;
import javax.xml.ws.Endpoint;

@WebService(endpointInterface = "Assignment5.Exercise2.MathInterfaceWS")
public class MathWSServer implements MathInterfaceWS {
    @Override
    public double add(double a, double b) {
        return a + b;
    }

    @Override
    public double subtract(double a, double b) {
        return a - b;
    }

    @Override
    public double multiply(double a, double b) {
        return a * b;
    }

    @Override
    public double divide(double a, double b) {
        return a / b;
    }


    public static void main(String[] args) {
        Endpoint.publish("http://localhost:56789/ws/mathwsserver", new MathWSServer());
        System.out.println("Server is now running...");
    }
}
