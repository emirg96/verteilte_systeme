package Assignment4.Exercise2;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class HotelClient {

    public static void main(String[] args) throws RemoteException, NotBoundException {

        Registry registry = LocateRegistry.getRegistry(RoomManager.REMOTE_HOST, RoomManager.REMOTE_PORT);
        RoomManager roomManager = (RoomManager) registry.lookup(RoomManager.REMOTE_ID);

        // Test code in IDE. Uncomment this and comment whole try catch to work!
        /*System.out.println("SUMMARY 1: " + roomManager.summary());

        for (int i = 0; i < 5; i++) {
            System.out.println(roomManager.book(3, 1, 20));
            if (i % 2 != 0) {
                System.out.println(roomManager.book(3, 10, 25));
            }
            System.out.println(roomManager.book(3, 23, 45));
        }

        System.out.println("SUMMARY 2: " + roomManager.summary());*/


        try {
            switch (args[0]) {
                case "checkAvailability":
                    System.out.println(roomManager.checkAvailability(Integer.parseInt(args[1]), Integer.parseInt(args[2])));
                    break;
                case "book":
                    System.out.println(roomManager.book(Integer.parseInt(args[1]), Integer.parseInt(args[2]), Integer.parseInt(args[3])));
                    break;
                case "summary":
                    System.out.println(roomManager.summary());
                default:
                    System.out.println("Unknown command! Available commands are:");
                    System.out.println("checkAvailability <arrivalDay> <departureDay>");
                    System.out.println("book <roomType> <arrivalDay> <departureDay>");
                    System.out.println("summary:");
            }
        } catch (Exception e) {
            System.out.println("There was an error in your request!");
            System.out.println("Available commands are:");
            System.out.println("checkAvailability <arrivalDay> <departureDay>");
            System.out.println("book <roomType> <arrivalDay> <departureDay>");
            System.out.println("summary:");
        }

    }
}
