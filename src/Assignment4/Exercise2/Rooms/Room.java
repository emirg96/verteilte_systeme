package Assignment4.Exercise2.Rooms;

import java.util.ArrayList;

public class Room {
    private int room_type;
    private int price;
    private int room_number;
    private ArrayList<Booking> bookings;

    public Room(int room_type, int price, int room_number) {
        this.room_type = room_type;
        this.price = price;
        this.room_number = room_number;
        this.bookings = new ArrayList<>();
    }

    public boolean checkAvailability(int arrival_day, int departure_day) {
        boolean available = true;

        for (Booking booking : bookings) {
            if (booking.check(arrival_day, departure_day)) {
                available = false;
            }
        }

        return available;
    }

    public String book(int arrival_day, int departure_day) {
        if (checkAvailability(arrival_day, departure_day)) {
            bookings.add(new Booking(arrival_day, departure_day));
            return "Successfully booked room " + room_type + "." + room_number + "!";
        } else
            return "No room available!";
    }

    public String summary() {
        String result = "Room " + room_type + "." + room_number + " bookings: ";

        if (bookings.size() == 0)
            result += " no bookings";
        else {
            for (Booking booking : bookings) {
                result += "\n\tFrom day: " + booking.getArrivalDay() + " - Until day: " + booking.getDepartureDay();
            }
        }

        return result;
    }
}
