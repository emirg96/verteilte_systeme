package Assignment4.Exercise2.Rooms;

import java.util.ArrayList;

public class RoomType {

    private int type;
    private int pricePerNight;

    private ArrayList<Room> rooms;

    public RoomType(int type, int numberOfRooms, int pricePerNight) {
        this.type = type;
        this.pricePerNight = pricePerNight;
        this.rooms = new ArrayList<>(numberOfRooms);
        for (int i = 0; i < numberOfRooms; i++) {
            rooms.add(new Room(type, pricePerNight, i));
        }
    }

    public boolean checkAvailability(int arrival_day, int departure_day) {

        for (Room room : rooms) {
            if (room.checkAvailability(arrival_day, departure_day)) {
                return true;
            }
        }

        return false;
    }

    public String book(int arrival_day, int departure_day) {

        for (Room room : rooms) {
            if (room.checkAvailability(arrival_day, departure_day)) {
                return room.book(arrival_day, departure_day);
            }
        }

        return "No rooms available!";
    }

    public String summary() {
        String result = "";

        for (Room room : rooms) {
            result += "\n" + room.summary();
        }

        return result;
    }

    public int getType() {
        return type;
    }

    public int getPricePerNight() {
        return pricePerNight;
    }
}
