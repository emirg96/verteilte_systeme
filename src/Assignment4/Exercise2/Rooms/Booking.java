package Assignment4.Exercise2.Rooms;

public class Booking {

    private int arrivalDay;
    private int departureDay;

    public Booking(int arrival_day, int departure_day) {
        this.arrivalDay = arrival_day;
        this.departureDay = departure_day;
    }

    public int getArrivalDay() {
        return arrivalDay;
    }

    public int getDepartureDay() {
        return departureDay;
    }

    public boolean check(int arrival_day, int departure_day) {
        return (this.arrivalDay <= departure_day && this.departureDay >= arrival_day);
    }
}
