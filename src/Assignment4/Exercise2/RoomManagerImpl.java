package Assignment4.Exercise2;

import Assignment4.Exercise2.Rooms.RoomType;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

public class RoomManagerImpl extends UnicastRemoteObject implements RoomManager {
    ArrayList<RoomType> roomTypes = new ArrayList<>(5);

    public RoomManagerImpl(int remotePort) throws RemoteException {
        super(remotePort);

        roomTypes.add(new RoomType(0, 10, 55));
        roomTypes.add(new RoomType(1, 20, 75));
        roomTypes.add(new RoomType(2, 5, 90));
        roomTypes.add(new RoomType(3, 3, 130));
        roomTypes.add(new RoomType(4, 2, 250));
    }

    @Override
    public String checkAvailability(int arrival_day, int departure_day) throws RemoteException {
        if (arrival_day < 1 || departure_day > 100 || arrival_day > departure_day)
            return "Invalid days range. 1 - 100 available!";

        String result = "Available rooms status for period: " + arrival_day + " - " + departure_day + "\n";

        for (RoomType roomType : roomTypes) {
            result += "Room type " + roomType.getType() + " : ";

            if (roomType.checkAvailability(arrival_day, departure_day)) {
                result += "available\n";
            } else {
                result += "not available\n";
            }
        }

        return result;
    }

    @Override
    public String book(int room_type, int arrival_day, int departure_day) throws RemoteException {
        for (RoomType roomType : roomTypes) {
            if (roomType.getType() == room_type)
                if (roomType.checkAvailability(arrival_day, departure_day)) {
                    return roomType.book(arrival_day, departure_day);
                }
        }
        return "Sorry, everything already booked!";
    }

    @Override
    public String summary() throws RemoteException {
        String result = "Room status summary:";

        for (RoomType roomType : roomTypes) {
            result += "\n" + roomType.summary();
        }

        return result;
    }
}
