package Assignment4.Exercise2;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RoomManager extends Remote {

    int REMOTE_PORT = 55557;
    String REMOTE_HOST = "localhost";
    String REMOTE_ID = "StringOpsServer";

    String checkAvailability(int arrival_day, int departure_day) throws RemoteException;

    String book(int room_type, int arrival_day, int departure_day) throws RemoteException;

    String summary() throws RemoteException;
}
