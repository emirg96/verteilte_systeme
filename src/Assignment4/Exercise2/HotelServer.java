package Assignment4.Exercise2;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class HotelServer {

    public static void main(String[] args) throws RemoteException, AlreadyBoundException {

        RoomManager roomManager = new RoomManagerImpl(RoomManager.REMOTE_PORT);
        Registry registry = LocateRegistry.createRegistry(RoomManager.REMOTE_PORT);
        registry.bind(RoomManager.REMOTE_ID, roomManager);

    }
}
