package Assignment4.Exercise1;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface DiningPhilosophersInterface extends Remote {

    int REMOTE_PORT = 55557;
    String REMOTE_HOST = "localhost";
    String REMOTE_ID = "DiningPhilosophers";

    long MAX_FORK_OCCUPIED_TIME = 10 * 1000; //

    String getASeat() throws RemoteException;

    void giveMeTheFork(String philosophersID) throws RemoteException, IllegalArgumentException;

    boolean eat() throws RemoteException;

    void leaveTheFork(String philosophersID) throws RemoteException;

}
