package Assignment4.Exercise1;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DiningPhilosophersServer extends UnicastRemoteObject implements DiningPhilosophersInterface {

    private String previousPhilosopherWithTheFork;
    private String philosopherWithTheFork;
    private List<String> philosophers;

    public DiningPhilosophersServer(int port) throws RemoteException {
        super(port);
        philosopherWithTheFork = null;
        long tookTheForkAtTime = 0l;
        philosophers = new ArrayList<>();
    }

    @Override
    public String getASeat() throws RemoteException {

        String uniqueID;
        do {
            uniqueID = generateRandomKey();
        } while (philosophers.contains(uniqueID));

        philosophers.add(uniqueID);

        return uniqueID;
    }

    @Override
    public void giveMeTheFork(String philosophersID) throws RemoteException, IllegalArgumentException {
        if (!philosophers.contains(philosophersID))
            throw new IllegalArgumentException("Take a seat first!");

        if (philosopherWithTheFork != null)
            philosopherWithTheFork = philosophersID;
    }

    @Override
    public boolean eat() throws RemoteException {
        // false if the philosopher has no seat or has no fork!
        return false;
    }

    @Override
    public void leaveTheFork(String philosophersID) throws RemoteException {

    }

    private static String generateRandomKey() {
        char randomChars[] = new char[16];

        for (int i = 0; i < randomChars.length; i++) {
            randomChars[i] = (char) (Math.random() * (122 - 97) + 1 + 97);
        }

        return Arrays.toString(randomChars);
    }

    public static void main(String[] args) throws RemoteException, AlreadyBoundException {
        DiningPhilosophersServer diningPhilosophersServer = new DiningPhilosophersServer(REMOTE_PORT);
        Registry registry = LocateRegistry.createRegistry(REMOTE_PORT);
        registry.bind(REMOTE_ID, diningPhilosophersServer);
    }
}
