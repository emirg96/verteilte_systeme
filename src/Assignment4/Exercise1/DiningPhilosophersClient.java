package Assignment4.Exercise1;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class DiningPhilosophersClient {

    public static void main(String[] args) throws RemoteException, NotBoundException {

        Registry registry = LocateRegistry.getRegistry(DiningPhilosophersInterface.REMOTE_HOST, DiningPhilosophersInterface.REMOTE_PORT);
        DiningPhilosophersInterface diningPhilosophers = (DiningPhilosophersInterface) registry.lookup(DiningPhilosophersInterface.REMOTE_ID);
    }
}
