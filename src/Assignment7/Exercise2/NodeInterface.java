package Assignment7.Exercise2;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface NodeInterface extends Remote {

    int REMOTE_PORT = 55123; // Starting port
    String REMOTE_HOST = "localhost";
    String REMOTE_ID = "Peer_"; // Peer_123456789
    String START_ID = "1000000000";

    NodeInterface getRightNeighbour() throws RemoteException;

    void setRightNeighbour(NodeInterface rightNeighbour) throws RemoteException;

    NodeInterface getLeftNeighbour() throws RemoteException;

    void setLeftNeighbour(NodeInterface leftNeighbour) throws RemoteException;

    int getRemotePort() throws RemoteException;

    String getRemoteHost() throws RemoteException;

    NodeInterface getNode() throws RemoteException;

    boolean checkAlive() throws RemoteException;

    NodeInterface getLastPeerLeft(String initiator) throws RemoteException;

    NodeInterface getLastPeerRight(String initiator) throws RemoteException;

    int countPeersRight(String IDs) throws RemoteException;

    int countPeersLeft(String IDs) throws RemoteException;
}
