package Assignment7.Exercise2;

import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * Peer2Peer Network. Ring topology. Automatic recovery on another peer's crash or leave.
 * Requires super node to enter the network. Super node is the one which accepts new peers.
 * If no super node exists or super node has crashed the new connected node will act as super node.
 * In case that super node crashes or leaves the ring, the remaining peers are still able to communicate
 * and leave the ring normally but no one anymore is able to join the ring because the super node is missing.
 * The next node which tries to connect will be new super node and therefore it will be the one which
 * accepts new peers (without ever affecting the previous ring/s which was/were left without a super node).
 * Peer count update every 5 seconds. Network failure detection every second. Tested on 10 peers. OK.
 * TODO: Detect when super node is missing and try to reconnect to it or the new super node.
 */
public class Node extends UnicastRemoteObject implements NodeInterface {

    private static int instanceID = 0; // Used for creating new instances of Node because not more than one node can be assigned to single port!

    private boolean amSuperNode;
    private int remotePort;
    private String remoteHost;
    private NodeInterface remoteNode;
    private NodeInterface left;
    private NodeInterface right;

    private Node(int port) throws RemoteException {
        super(port);
    }

    public Node() throws RemoteException, AlreadyBoundException, InterruptedException {
        NodeInterface node;

        if ((node = connectToMainNode()) == null) { // Super node doesn't exist! Create one.

            remoteNode = startServer(remoteHost = START_ID, remotePort = REMOTE_PORT);
            remoteNode.setLeftNeighbour(this);
            remoteNode.setRightNeighbour(this);

            amSuperNode = true;

        } else { // Super node exists! Create new node and connect it to super node.

            boolean created = false;

            while (!created)
                try {
                    remoteNode = startServer(remoteHost = getRandomID(), remotePort = REMOTE_PORT + ++instanceID);
                    created = true;
                } catch (Exception e) {
                }

            setRightNeighbour(node); // New node's right neighbour is ALWAYS the super node.

            if (node.getLeftNeighbour() != node) { // Super node has already left neighbour.
                setLeftNeighbour(node.getLeftNeighbour()); // New node's left neighbour is super node's left neighbour.
                try {
                    node.getLeftNeighbour().setRightNeighbour(this); // Update super node's left neighbour's right neighbour to the new node.
                } catch (Exception e) {
                }
            } else { // Super node hasn't left neighbour.
                setLeftNeighbour(node); // New node's left neighbour is the super node (as well as new node's right neighbour).
                node.setRightNeighbour(this); // Super node's right neighbour is the new node itself.
            }

            node.setLeftNeighbour(this); // Super node's left neighbour is ALWAYS the new node itself.
        }

        checkIfNeighboursAreAlive(); // Start a timer which checks periodically if the new node's neighbours are still alive.
    }

    /**
     * Connect to node with given remote ID and port.
     *
     * @param remoteNodeID
     * @param remoteNodePort
     * @return
     * @throws RemoteException
     * @throws NotBoundException
     */
    NodeInterface connectToNode(String remoteNodeID, int remoteNodePort) throws RemoteException, NotBoundException {
        Registry registry = LocateRegistry.getRegistry(NodeInterface.REMOTE_HOST, NodeInterface.REMOTE_PORT);
        NodeInterface node = (NodeInterface) registry.lookup(remoteNodeID);

        return node;
    }

    /**
     * Start new server with given remote ID and port.
     *
     * @param withRemoteID
     * @param withRemotePort
     * @return
     * @throws RemoteException
     * @throws AlreadyBoundException
     */
    Node startServer(String withRemoteID, int withRemotePort) throws RemoteException, AlreadyBoundException {
        Node node = new Node(withRemotePort);
        node.remoteHost = withRemoteID;
        node.remotePort = withRemotePort;
        node.remoteNode = node;
        Registry registry = LocateRegistry.createRegistry(withRemotePort);
        registry.bind(REMOTE_ID + withRemoteID, node);

        return node;
    }

    /**
     * Try to connect to main node. If main node is not reachable or doesn't exist null is returned.
     *
     * @return
     */
    private NodeInterface connectToMainNode() {
        NodeInterface node = null;

        try {
            node = connectToNode(REMOTE_ID + START_ID, REMOTE_PORT);
        } catch (Exception e) {

        }

        return node;
    }

    /**
     * Generate random ID (for remote host ID).
     *
     * @return
     */
    private static String getRandomID() {
        String result = "";
        int numDigits = 10;

        for (int i = 0; i < numDigits; i++) {
            result += (char) (Math.random() * (57 - 47) + 1 + 47);
        }

        return result;
    }

    @Override
    public NodeInterface getRightNeighbour() throws RemoteException {

        // DETECT DEAD LOCK HERE !!!
        if (amSuperNode)
            return remoteNode.getRightNeighbour();

        return right;
    }

    @Override
    public void setRightNeighbour(NodeInterface rightNeighbour) throws RemoteException {

        if (amSuperNode)
            remoteNode.setRightNeighbour(rightNeighbour);

        right = rightNeighbour;

        onUpdate();
    }

    @Override
    public NodeInterface getLeftNeighbour() throws RemoteException {

        if (amSuperNode)
            return remoteNode.getLeftNeighbour();

        return left;
    }

    @Override
    public void setLeftNeighbour(NodeInterface leftNeighbour) throws RemoteException {

        if (amSuperNode)
            remoteNode.setLeftNeighbour(leftNeighbour);

        left = leftNeighbour;

        onUpdate();
    }

    @Override
    public int getRemotePort() throws RemoteException {
        return remotePort;
    }

    @Override
    public String getRemoteHost() throws RemoteException {
        return remoteHost;
    }

    @Override
    public NodeInterface getNode() throws RemoteException {
        return remoteNode;
    }

    @Override
    public boolean checkAlive() throws RemoteException {
        return true;
    }

    @Override
    public NodeInterface getLastPeerLeft(String initiator) throws RemoteException {

        if (getLeftNeighbour() == this) {
            return this;
        }

        try {
            return getLeftNeighbour().getLastPeerLeft(remoteHost);
        } catch (Exception e) {
            return this;
        }

        // Wrongly setting left peer as itself and exception throws when only one peer left!
    }

    @Override
    public NodeInterface getLastPeerRight(String initiator) throws RemoteException {

        if (getRightNeighbour() == this) {
            return this;
        }

        try {
            return getRightNeighbour().getLastPeerRight(remoteHost);
        } catch (Exception e) {
            return this;
        }
    }

    @Override
    public int countPeersRight(String IDs) throws RemoteException {
        if (IDs.contains(remoteHost)) {
            return IDs.split(">").length;
        } else {
            return getRightNeighbour().countPeersRight(IDs + ">" + remoteHost);
        }
    }

    @Override
    public int countPeersLeft(String IDs) throws RemoteException {
        if (IDs.contains(remoteHost)) {
            return IDs.split("<").length;
        } else {
            return getLeftNeighbour().countPeersLeft(IDs + "<" + remoteHost);
        }
    }

    private void onUpdate() throws RemoteException {

        String leftNeighbour = "null";
        String rightNeighbour = "null";

        try {
            leftNeighbour = getLeftNeighbour() == null ? leftNeighbour : getLeftNeighbour().getRemoteHost();
        } catch (Exception e) {
        }

        try {
            rightNeighbour = getRightNeighbour() == null ? rightNeighbour : getRightNeighbour().getRemoteHost();
        } catch (Exception e) {
        }

        if (!leftNeighbour.equals("null") && !rightNeighbour.equals("null")) {
            System.out.println("Node " + remotePort + ": " + getRemoteHost() + ": left = " + leftNeighbour + " & right = " + rightNeighbour);
        }
    }

    /**
     * Checks every second for dead neighbours and tries to fix the ring.
     * Shows peer count update every 5 seconds.
     *
     * @throws InterruptedException
     * @throws RemoteException
     */
    private void checkIfNeighboursAreAlive() throws InterruptedException, RemoteException {
        boolean leftAlive;
        boolean rightAlive;
        int updateCount = 0;

        while (true) {
            Thread.sleep(1000);

            try {
                getLeftNeighbour().checkAlive();
                leftAlive = true;
            } catch (Exception e) {
                leftAlive = false;
            }

            try {
                getRightNeighbour().checkAlive();
                rightAlive = true;
            } catch (Exception e) {
                rightAlive = false;
            }

            if (!leftAlive && !rightAlive) {
                setLeftNeighbour(this);
                setRightNeighbour(this);

                if (!amSuperNode) {
                    System.out.println("Both left and right neighbour died :(. No reason to live anymore. Commiting suicide...");
                    System.exit(0);
                }
            }

            if (!leftAlive) {
                setLeftNeighbour(getLastPeerRight(remoteHost));
                leftAlive = true;
            }

            if (!rightAlive) {
                setRightNeighbour(getLastPeerLeft(remoteHost));
                rightAlive = true;
            }

            // Count peers update
            int leftCount = 0, rightCount = 0;

            try {
                leftCount = getLeftNeighbour().countPeersLeft(remoteHost);
            } catch (Exception e) {
            }

            try {
                rightCount = getRightNeighbour().countPeersRight(remoteHost);
            } catch (Exception e) {
            }

            if (updateCount++ % 5 == 0) // Show update every three seconds.
                System.out.println("-> left peer count = " + leftCount + " & right peer count = " + rightCount + " => " + (leftCount == rightCount ? "OK" : "INCONSISTENT"));

        }
    }
}
