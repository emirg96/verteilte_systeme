package Assignment1.Exercise4;

import java.util.ArrayList;
import java.util.Arrays;

public class MathProtocol {

    /**
     * Processes the input and gives back the result if valid request is given.
     *
     * @param input
     * @return
     */
    public static String processInput(String input) {
        String function;
        String number;

        try {
            function = input.split(";")[0];
            number = input.split(";")[1];
        } catch (Exception e) {
            return "Invalid request!";
        }

        try {
            switch (function) {
                case "primes":
                    return Arrays.toString(getPrimesToNumber(Integer.parseInt(number))).toString();
                case "perimeter":
                    return getCirclePerimeter(Double.parseDouble(number)) + "";
                case "sqrt":
                    return squareRoot(Integer.parseInt(number)) + "";
                default:
                    return "Invalid function!";
            }
        } catch (Exception e) {
            return "Invalid parameter!";
        }

    }

    /**
     * Returns an array of prime numbers from 0 up to given number.
     *
     * @param number
     * @return
     */
    private static int[] getPrimesToNumber(int number) {

        ArrayList<Integer> result = new ArrayList<>();

        for (int i = 0; i <= number; i++) {
            if (isPrime(i)) {
                result.add(i);
            }
        }

        return result.stream().mapToInt(i -> i).toArray();
    }

    /**
     * Checks if given number is prime or not.
     *
     * @param n
     * @return
     */
    private static boolean isPrime(int n) {
        if (n % 2 == 0) return false;
        for (int i = 3; i * i <= n; i += 2) {
            if (n % i == 0)
                return false;
        }
        return true;
    }

    /**
     * Calculates a perimeter of a circle with given radius.
     *
     * @param radius
     * @return
     */
    private static double getCirclePerimeter(double radius) {
        return Math.PI * radius * 2;
    }

    /**
     * Calculates square root of given integer.
     *
     * @param number
     * @return
     */
    private static double squareRoot(int number) {
        return Math.sqrt(number);
    }

}
