package Assignment1.Exercise4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class MathClient implements Runnable {

    private String host;
    private int port;

    private String function;
    private String number;

    private MathClient() {
    }

    /**
     * Creates new client socket and sends the query
     *
     * @param address
     * @param function
     * @param number
     */
    public MathClient(String address, String function, int number) {
        this.host = address.split(":")[0];
        this.port = Integer.parseInt(address.split(":")[1]);
        this.function = function;
        this.number = Integer.toString(number);

        Thread thread = new Thread(this);
        thread.start();
    }

    /**
     * Creates new client socket and sends the query
     *
     * @param address
     * @param function
     * @param radius
     */
    public MathClient(String address, String function, double radius) {
        this.host = address.split(":")[0];
        this.port = Integer.parseInt(address.split(":")[1]);
        this.function = function;
        this.number = Double.toString(radius);

        Thread thread = new Thread(this);
        thread.start();
    }

    /**
     * Connects to server, sends message and prints given server output
     */
    @Override
    public void run() {
        try {
            Socket socket = new Socket(this.host, this.port);

            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

            out.println(this.function + ";" + this.number);

            String inputLine;
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            if ((inputLine = in.readLine()) != null) {
                System.out.println("MathClient: Answer from server on query '" + function + " " + number + "': " + inputLine);
            }

            socket.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
