package Assignment1.Exercise4;

public class Main {

    /**
     * Test/Run
     *
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {

        MathServer mathServer = new MathServer(5555);


        Thread.sleep(1000);

        // Valid requests
        MathClient mathClient0 = new MathClient("localhost:5555", "primes", 100);
        MathClient mathClient1 = new MathClient("localhost:5555", "perimeter", 1.78);
        MathClient mathClient2 = new MathClient("localhost:5555", "perimeter", 3);
        MathClient mathClient3 = new MathClient("localhost:5555", "sqrt", 144);

        Thread.sleep(1000);

        // Invalid requests
        MathClient mathClient4 = new MathClient("localhost:5555", "primes", 100.00);
        MathClient mathClient5 = new MathClient("localhost:5555", "sqrt", 144.00);
        MathClient mathClient6 = new MathClient("localhost:5555", "unknown", 144.00);
        MathClient mathClient7 = new MathClient("localhost:5555", "unknown", 144);
    }

}
