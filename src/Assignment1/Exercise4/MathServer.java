package Assignment1.Exercise4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class MathServer implements Runnable {

    private int port;
    private ServerSocket serverSocket;
    private Socket client;

    private MathServer() {
    }

    /**
     * Creates new thread which will listen to new client/s using the already created ServerSocket
     *
     * @param serverSocket
     */
    private MathServer(ServerSocket serverSocket) {
        this.serverSocket = serverSocket;

        Thread thread = new Thread(this);
        thread.start();
    }

    /**
     * Creates new ServerSocket at given port
     *
     * @param port
     */
    public MathServer(int port) {
        this.port = port;

        try {
            this.serverSocket = new ServerSocket(this.port);

            Thread thread = new Thread(this);
            thread.start();

        } catch (Exception e) {
            System.out.println("MathServer: Couldn't start the server at this port: " + this.port);
        }
    }

    /**
     * Accepts incoming connection, creates new thread for listening new incoming connections and processes received message
     */
    @Override
    public void run() {
        try {
            this.client = serverSocket.accept();

            // Listen to a new client after accepting the current one
            MathServer mathServer = new MathServer(serverSocket);

            BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
            PrintWriter out = new PrintWriter(client.getOutputStream(), true);

            String inputLine;

            if ((inputLine = in.readLine()) != null) {
                out.println(MathProtocol.processInput(inputLine));
            }

            this.client.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
