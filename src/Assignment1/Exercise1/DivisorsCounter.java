package Assignment1.Exercise1;

public class DivisorsCounter {

    private final static int FROM_NUMBER = 1;
    private final static int TO_NUMBER = 100000;

    private static int NUMBER = 1;
    private static int DIVISORS_COUNT = 1;

    public static void main(String[] args) {

        long start_time = System.nanoTime();

        for (int i = FROM_NUMBER; i <= TO_NUMBER; i++) {

            int count;

            if ((count = getNumberOfDivisors(i)) > DIVISORS_COUNT) {
                NUMBER = i;
                DIVISORS_COUNT = count;
            }

        }

        System.out.println("Finished after " + (System.nanoTime() - start_time) + " ns");
        System.out.println("The (first) number with most divisors in the range from " + FROM_NUMBER + " to " + TO_NUMBER + " is the number " + NUMBER + " with " + DIVISORS_COUNT + " divisors.");

        /*
        Finished after 12336558713 ns
        The (first) number with most divisors in the range from 1 to 100000 is the number 83160 with 128 divisors.
         */

    }

    public static int getNumberOfDivisors(int i) {
        int divisors_count = 1;

        if (i > 2)
            divisors_count++;

        for (int j = 2; j <= i / 2 + 1; j++)
            if (i % j == 0)
                divisors_count++;

        return divisors_count;
    }

}
