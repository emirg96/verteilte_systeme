package Assignment1.Exercise1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class DivisorsCounterThreaded implements Runnable {

    private final static int FROM_NUMBER = 1;
    private final static int TO_NUMBER = 100000;

    private final static int NUM_THREADS = 10;

    private static HashMap<Integer, Integer> results = new HashMap<Integer, Integer>(NUM_THREADS);

    private int NUMBER = 1;
    private int DIVISORS_COUNT = 1;

    private int start;
    private int end;

    private DivisorsCounterThreaded() {
    }

    public DivisorsCounterThreaded(int start, int end) {
        this.start = start;
        this.end = end;
    }

    public static void main(String[] args) throws InterruptedException {

        long start_time = System.nanoTime();

        ArrayList<Thread> threads = new ArrayList<Thread>(NUM_THREADS);

        int start = FROM_NUMBER;
        int end;

        for (int i = NUM_THREADS; i >= 1; i--) {

            if (i == 1)
                end = TO_NUMBER;
            else
                end = start + (TO_NUMBER - FROM_NUMBER) / NUM_THREADS;

            DivisorsCounterThreaded divisorsCounterThreaded = new DivisorsCounterThreaded(start, end);
            Thread thread = new Thread(divisorsCounterThreaded);
            thread.start();

            threads.add(thread);

            start = end + 1;
        }

        for (Thread thread : threads) {
            thread.join();
        }

        int number = 1, divisors_count = 1;
        for (Map.Entry<Integer, Integer> pair : results.entrySet()) {
            if (pair.getValue() > divisors_count) {
                number = pair.getKey();
                divisors_count = pair.getValue();
            }
        }

        System.out.println("Finished after " + (System.nanoTime() - start_time) + " ns");
        System.out.println("The (first) number with most divisors in the range from " + FROM_NUMBER + " to " + TO_NUMBER + " is the number " + number + " with " + divisors_count + " divisors.");

        /*
        Finished after 6375238363 ns
        The (first) number with most divisors in the range from 1 to 100000 is the number 83160 with 128 divisors.
         */

    }

    @Override
    public void run() {

        for (int i = this.start; i <= this.end; i++) {

            int count;

            if ((count = DivisorsCounter.getNumberOfDivisors(i)) > DIVISORS_COUNT) {
                NUMBER = i;
                DIVISORS_COUNT = count;
            }

        }

        results.put(NUMBER, DIVISORS_COUNT);
    }

}
