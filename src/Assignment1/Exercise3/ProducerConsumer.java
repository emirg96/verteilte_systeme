package Assignment1.Exercise3;

import Assignment1.Exercise1.DivisorsCounter;

import java.util.concurrent.*;

public class ProducerConsumer implements Runnable {

    private final static int FROM_NUMBER = 1;
    private final static int TO_NUMBER = 100000;
    private static final int MAX_THREADS = 10;

    private int number;
    private int divisorsCount = 0;
    private LinkedBlockingQueue<ProducerConsumer> queue;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getDivisorsCount() {
        return divisorsCount;
    }

    public void setDivisorsCount(int divisorsCount) {
        this.divisorsCount = divisorsCount;
    }

    public ProducerConsumer(int number, LinkedBlockingQueue<ProducerConsumer> queue) {
        this.number = number;
        this.queue = queue;
    }

    private ProducerConsumer() {
    }

    public void run() {
        this.divisorsCount = DivisorsCounter.getNumberOfDivisors(this.number);
        this.queue.add(this);
    }

    public static void main(String[] args) throws InterruptedException {

        long start_time = System.nanoTime();

        ExecutorService pool = Executors.newFixedThreadPool(MAX_THREADS);
        ConcurrentLinkedQueue<ProducerConsumer> queue = new ConcurrentLinkedQueue<ProducerConsumer>();
        LinkedBlockingQueue<ProducerConsumer> resultQueue = new LinkedBlockingQueue<>(TO_NUMBER - FROM_NUMBER + 1);

        // Add elements to the queue
        for (int i = FROM_NUMBER; i <= TO_NUMBER; i++) {
            ProducerConsumer producerConsumer = new ProducerConsumer(i, resultQueue);
            queue.add(producerConsumer);
        }

        // Execute the added elements
        while (!queue.isEmpty()) {
            pool.execute(queue.poll());
        }

        // Send shutdown signal but wait Long.MAX_VALUE milliseconds until force-shutdown
        pool.shutdown();
        pool.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);

        // Compare results and take (only) the first number with most divisors (this could be improved)
        int number = 1, divisorsCount = 1;
        for (ProducerConsumer producerConsumer : resultQueue) {
            if (divisorsCount < producerConsumer.getDivisorsCount()) {
                number = producerConsumer.getNumber();
                divisorsCount = producerConsumer.getDivisorsCount();
            }
        }

        System.out.println("Finished after " + (System.nanoTime() - start_time) + " ns");
        System.out.println("The (first) number with most divisors in the range from " + FROM_NUMBER + " to " + TO_NUMBER + " is the number " + number + " with " + divisorsCount + " divisors.");

        /*
        Finished after 5530552719 ns
        The (first) number with most divisors in the range from 1 to 100000 is the number 83160 with 128 divisors.
         */

    }
}
