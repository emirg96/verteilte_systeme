package Assignment8.Exercise2;

import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class Node extends UnicastRemoteObject implements NodeInterface {

    private static int instanceID = 0; // Used for creating new instances of Node because not more than one node can be assigned to single port!

    private boolean amSuperNode;
    private int myPort;
    private String myID;
    private NodeInterface myNode;
    private NodeInterface left;
    private NodeInterface right;

    private Node(int port) throws RemoteException {
        super(port);
    }

    protected Node() throws RemoteException, AlreadyBoundException, InterruptedException {
        NodeInterface node;

        if ((node = connectToMainNode()) == null) { // 'Main' node doesn't exist! Create one.

            myNode = startServer(myID = START_ID, myPort = REMOTE_PORT);
            myNode.setLeftNeighbour(this);
            myNode.setRightNeighbour(this);

            amSuperNode = true;

        } else { // 'Main' node exists! Connect to it.

            boolean created = false;

            while (!created)
                try {
                    myNode = startServer(myID = getRandomID(), myPort = REMOTE_PORT + instanceID++);
                    created = true;
                } catch (Exception e) {
                }

            setRightNeighbour(node);

            if (node.getLeftNeighbour() != node) {
                setLeftNeighbour(node.getLeftNeighbour());
                try {
                    node.getLeftNeighbour().setRightNeighbour(this);
                } catch (Exception e) {

                }
            } else {
                setLeftNeighbour(node);
                node.setRightNeighbour(this);
            }

            node.setLeftNeighbour(this);
        }

        checkIfNeighboursAreAlive();
    }

    NodeInterface connectToNode(String remoteNodeID, int remoteNodePort) throws RemoteException, NotBoundException {
        Registry registry = LocateRegistry.getRegistry(NodeInterface.REMOTE_HOST, NodeInterface.REMOTE_PORT);
        NodeInterface node = (NodeInterface) registry.lookup(remoteNodeID);

        return node;
    }

    Node startServer(String withRemoteID, int withRemotePort) throws RemoteException, AlreadyBoundException {
        Node node = new Node(withRemotePort);
        node.myID = withRemoteID;
        node.myPort = withRemotePort;
        node.myNode = node;
        Registry registry = LocateRegistry.createRegistry(withRemotePort);
        registry.bind(REMOTE_ID + withRemoteID, node);

        return node;
    }

    private NodeInterface connectToMainNode() {
        NodeInterface node = null;

        try {
            node = connectToNode(REMOTE_ID + START_ID, REMOTE_PORT);
        } catch (Exception e) {

        }

        return node;
    }

    private static String getRandomID() {
        String result = "";
        int numDigits = 10;

        for (int i = 0; i < numDigits; i++) {
            result += (char) (Math.random() * (57 - 47) + 1 + 47);
        }

        return result;
    }

    @Override
    public NodeInterface getRightNeighbour() throws RemoteException {

        // DETECT DEAD LOCK HERE !!!
        if (amSuperNode)
            return myNode.getRightNeighbour();

        return right;
    }

    @Override
    public void setRightNeighbour(NodeInterface rightNeighbour) throws RemoteException {

        if (amSuperNode)
            myNode.setRightNeighbour(rightNeighbour);

        right = rightNeighbour;

        onConnect();
    }

    @Override
    public NodeInterface getLeftNeighbour() throws RemoteException {

        if (amSuperNode)
            return myNode.getLeftNeighbour();

        return left;
    }

    @Override
    public void setLeftNeighbour(NodeInterface leftNeighbour) throws RemoteException {

        if (amSuperNode)
            myNode.setLeftNeighbour(leftNeighbour);

        left = leftNeighbour;

        onConnect();
    }

    @Override
    public int getRemotePort() throws RemoteException {
        return myPort;
    }

    @Override
    public String getRemoteHost() throws RemoteException {
        return myID;
    }

    @Override
    public NodeInterface getNode() throws RemoteException {
        return myNode;
    }

    @Override
    public boolean checkAlive() throws RemoteException {
        return true;
    }

    @Override
    public NodeInterface getLastPeerLeft(NodeInterface initiator) throws RemoteException {

        try {
            return getLeftNeighbour().getLastPeerLeft(this);
        } catch (Exception e) {
            return this;
        }

        // Wrongly setting left peer as itself and exception throws when only one peer left!
    }

    @Override
    public NodeInterface getLastPeerRight(NodeInterface initiator) throws RemoteException {
        try {
            return getRightNeighbour().getLastPeerRight(this);
        } catch (Exception e) {
            return this;
        }
    }

    private void onConnect() throws RemoteException {

        String leftNeighbour = "null";
        String rightNeighbour = "null";

        try {
            leftNeighbour = getLeftNeighbour() == null ? leftNeighbour : getLeftNeighbour().getRemoteHost();
        } catch (Exception e) {
        }

        try {
            rightNeighbour = getRightNeighbour() == null ? rightNeighbour : getRightNeighbour().getRemoteHost();
        } catch (Exception e) {
        }


        System.out.println("Node " + myPort + ": " + getRemoteHost() + ": left = " + leftNeighbour + " & right = " + rightNeighbour);
    }

    private void checkIfNeighboursAreAlive() throws InterruptedException, RemoteException {
        boolean leftAlive;
        boolean rightAlive;

        while (true) {
            Thread.sleep(1000);

            try {
                getLeftNeighbour().checkAlive();
                leftAlive = true;
            } catch (Exception e) {
                leftAlive = false;
            }

            try {
                getRightNeighbour().checkAlive();
                rightAlive = true;
            } catch (Exception e) {
                rightAlive = false;
            }

            if (!leftAlive && !rightAlive) {
                setLeftNeighbour(this);
                setRightNeighbour(this);
                System.out.println("Both left and right neighbour crashed. No way out.");
            }

            if (!leftAlive) {
                setLeftNeighbour(getLastPeerRight(this));
                leftAlive = true;
            }

            if (!rightAlive) {
                setRightNeighbour(getLastPeerLeft(this));
                rightAlive = true;
            }
        }

    }
}
